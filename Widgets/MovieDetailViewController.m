//
//  MovieDetailViewController.m
//  Widgets
//
//  Created by Hu Junfeng on 8/5/15.
//  Copyright (c) 2015 2359 Media Pte Ltd. All rights reserved.
//

#import "MovieDetailViewController.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "MovieDetailViewModel.h"
#import "Movie.h"

@interface MovieDetailViewController ()

@property (weak, nonatomic) IBOutlet UILabel *movieTitleLabel;

@end

@implementation MovieDetailViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self bindViewModel];
}

- (void)bindViewModel
{
    self.title = self.viewModel.title;
    self.movieTitleLabel.text = self.viewModel.movie.title;
}

- (IBAction)showPlayer:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(movieDetailViewController:didSelectPlayerWithMovie:)]) {
        [self.delegate movieDetailViewController:self didSelectPlayerWithMovie:self.viewModel.movie];
    }
}

@end
