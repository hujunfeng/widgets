//
//  ListTableViewCell.m
//  Widgets
//
//  Created by Hu Junfeng on 12/5/15.
//  Copyright (c) 2015 2359 Media Pte Ltd. All rights reserved.
//

#import "ListTableViewCell.h"
#import "Movie.h"

@implementation ListTableViewCell

- (void)bindViewModel:(Movie *)viewModel
{
    self.textLabel.text = viewModel.title;
}

@end
