//
//  MovieDetailViewController.h
//  Widgets
//
//  Created by Hu Junfeng on 8/5/15.
//  Copyright (c) 2015 2359 Media Pte Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MovieDetailViewModel;
@class Movie;
@protocol MovieDetailViewControllerDelegate;

@interface MovieDetailViewController : UIViewController

@property (nonatomic) MovieDetailViewModel *viewModel;
@property (nonatomic, weak) id <MovieDetailViewControllerDelegate> delegate;

@end

@protocol MovieDetailViewControllerDelegate <NSObject>

- (void)movieDetailViewController:(MovieDetailViewController *)viewController didSelectPlayerWithMovie:(Movie *)movie;

@end
