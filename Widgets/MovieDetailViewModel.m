//
//  MovieDetailViewModel.m
//  Widgets
//
//  Created by Hu Junfeng on 13/5/15.
//  Copyright (c) 2015 2359 Media Pte Ltd. All rights reserved.
//

#import "MovieDetailViewModel.h"
#import "Movie.h"

@interface MovieDetailViewModel ()

@property (nonatomic, readwrite) Movie *movie;

@end

@implementation MovieDetailViewModel

- (instancetype)initWithModel:(id)model
{
    self = [super init];
    if (self) {
        NSParameterAssert([model isKindOfClass:[Movie class]]);
        _movie = model;
    }
    return self;
}

- (NSString *)title
{
    return self.movie.title;
}

@end
