//
//  AppDelegate.m
//  Widgets
//
//  Created by Hu Junfeng on 8/5/15.
//  Copyright (c) 2015 2359 Media Pte Ltd. All rights reserved.
//

#import "AppDelegate.h"
#import "RootWidget.h"
#import "IMDb.h"

@interface AppDelegate ()

@property (nonatomic) RootWidget *rootWidget;

@end

@implementation AppDelegate

- (RootWidget *)rootWidget
{
    if (!_rootWidget) {
        _rootWidget = [[RootWidget alloc] init];
    }
    return _rootWidget;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.window.rootViewController = self.rootWidget.initialViewController;
    [self.window makeKeyAndVisible];
    
    return YES;
}

@end
