//
//  MovieDetailViewModel.h
//  Widgets
//
//  Created by Hu Junfeng on 13/5/15.
//  Copyright (c) 2015 2359 Media Pte Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ViewModel.h"

@class Movie;

@interface MovieDetailViewModel : NSObject <ViewModel>

@property (nonatomic, readonly) NSString *title;
@property (nonatomic, readonly) Movie *movie;

@end
