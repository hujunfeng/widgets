//
//  MoviePlayerViewController.h
//  Widgets
//
//  Created by Hu Junfeng on 8/5/15.
//  Copyright (c) 2015 2359 Media Pte Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@class MoviePlayerViewModel;
@protocol MoviePlayerViewControllerDelegate;

@interface MoviePlayerViewController : UIViewController

@property (nonatomic) MoviePlayerViewModel *viewModel;
@property (nonatomic, weak) id <MoviePlayerViewControllerDelegate> delegate;

@end

@protocol MoviePlayerViewControllerDelegate <NSObject>

- (void)moviePlayerViewControllerDidClose:(MoviePlayerViewController *)viewController;

@end
