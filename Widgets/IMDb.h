//
//  IMDb.h
//  Widgets
//
//  Created by Hu Junfeng on 9/5/15.
//  Copyright (c) 2015 2359 Media Pte Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@class RACSignal;

@interface IMDb : NSObject

+ (instancetype)sharedStore;

- (void)fetchTopMoviesOnSuccess:(void (^)(NSArray *movies))success failure:(void (^)(NSError *error))failure;

- (RACSignal *)signalForTopMovies;

@end
