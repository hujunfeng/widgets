//
//  ListViewModel.h
//  Widgets
//
//  Created by Hu Junfeng on 9/5/15.
//  Copyright (c) 2015 2359 Media Pte Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ViewModel.h"

@interface ListViewModel : NSObject <ViewModel>

@property (nonatomic) NSString *title;
@property (nonatomic) NSArray *items;

@end
