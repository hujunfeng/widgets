//
//  ListTableViewCell.h
//  Widgets
//
//  Created by Hu Junfeng on 12/5/15.
//  Copyright (c) 2015 2359 Media Pte Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ReactiveTableViewBinding/CEReactiveView.h>

@interface ListTableViewCell : UITableViewCell <CEReactiveView>

@end
