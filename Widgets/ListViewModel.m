//
//  ListViewModel.m
//  Widgets
//
//  Created by Hu Junfeng on 9/5/15.
//  Copyright (c) 2015 2359 Media Pte Ltd. All rights reserved.
//

#import "ListViewModel.h"
#import <ReactiveCocoa/ReactiveCocoa.h>

@implementation ListViewModel

- (instancetype)initWithModel:(RACSignal *)model
{
    self = [super init];
    if (self) {
        NSParameterAssert([model isKindOfClass:[RACSignal class]]);
        _title = NSLocalizedString(@"List", nil);
        [model subscribeNext:^(NSArray *movies) {
            NSLog(@"movies: %@", movies);
            _items = movies;
        }];
    }
    return self;
}

@end
