//
//  IMDb.m
//  Widgets
//
//  Created by Hu Junfeng on 9/5/15.
//  Copyright (c) 2015 2359 Media Pte Ltd. All rights reserved.
//

#import "IMDb.h"
#import "Movie.h"
#import <ReactiveCocoa/ReactiveCocoa.h>

@implementation IMDb

+ (instancetype)sharedStore
{
    static dispatch_once_t pred;
    static id __singleton = nil;
    
    dispatch_once(&pred, ^{ __singleton = [[self alloc] init]; });
    return __singleton;
}

- (void)fetchTopMoviesOnSuccess:(void (^)(NSArray *movies))success failure:(void (^)(NSError *error))failure
{
    NSString *filename = @"imdb_top";
    NSURL *fileURL = [[NSBundle mainBundle] URLForResource:filename withExtension:@"json"];
    NSData *data = [NSData dataWithContentsOfURL:fileURL];
    NSError *error;
    NSDictionary *JSONObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
    if (!JSONObject) {
        NSLog(@"Error in parsing %@.json;\n%@", filename, [error localizedDescription]);
        if (failure) failure(error);
        return;
    }
    
    if (success) {
        NSMutableArray *movies = [NSMutableArray array];
        for (NSDictionary *dict in JSONObject[@"Top"]) {
            Movie *aMovie = [[Movie alloc] initWithDictionary:dict];
            [movies addObject:aMovie];
        }
     
        success(movies);
    }
}

- (RACSignal *)signalForTopMovies
{
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        [self fetchTopMoviesOnSuccess:^(NSArray *movies) {
            [subscriber sendNext:movies];
            [subscriber sendCompleted];
        } failure:^(NSError *error) {
            [subscriber sendError:error];
        }];
        return nil;
    }];
}

@end
