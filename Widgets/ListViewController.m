//
//  ViewController.m
//  Widgets
//
//  Created by Hu Junfeng on 8/5/15.
//  Copyright (c) 2015 2359 Media Pte Ltd. All rights reserved.
//

#import "ListViewController.h"
#import "ListViewModel.h"
#import "Movie.h"

#import <ReactiveCocoa/ReactiveCocoa.h>
#import <ReactiveTableViewBinding/CETableViewBindingHelper.h>

static NSString * const CellIdentifier = @"ListTableViewCell";

@interface ListViewController ()

@end

@implementation ListViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self bindViewModel];
}

- (void)bindViewModel
{
    self.title = self.viewModel.title;
    
    UINib *cellNib = [UINib nibWithNibName:CellIdentifier bundle:nil];
    
    CETableViewBindingHelper *helper = [CETableViewBindingHelper bindingHelperForTableView:self.tableView
                                                                              sourceSignal:RACObserve(self.viewModel, items)
                                                                          selectionCommand:nil
                                                                              templateCell:cellNib];
    helper.delegate = self;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.delegate respondsToSelector:@selector(listViewController:didSelectPlayerWithMovie:)]) {
        Movie *movie = self.viewModel.items[indexPath.row];
        [self.delegate listViewController:self didSelectPlayerWithMovie:movie];
    }
}

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath
{
    if ([self.delegate respondsToSelector:@selector(listViewController:didSelectDetailWithMovie:)]) {
        Movie *movie = self.viewModel.items[indexPath.row];
        [self.delegate listViewController:self didSelectDetailWithMovie:movie];
    }
}

@end
