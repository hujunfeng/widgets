//
//  MoviePlayerViewModel.m
//  Widgets
//
//  Created by Hu Junfeng on 13/5/15.
//  Copyright (c) 2015 2359 Media Pte Ltd. All rights reserved.
//

#import "MoviePlayerViewModel.h"
#import "Movie.h"

@interface MoviePlayerViewModel ()

@property (nonatomic) Movie *movie;

@end

@implementation MoviePlayerViewModel

- (instancetype)initWithModel:(id)model
{
    self = [super init];
    if (self) {
        NSParameterAssert([model isKindOfClass:[Movie class]]);
        _title = NSLocalizedString(@"Movie Player", nil);
        _movie = model;
    }
    return self;
}

@end
