//
//  ViewController.h
//  Widgets
//
//  Created by Hu Junfeng on 8/5/15.
//  Copyright (c) 2015 2359 Media Pte Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ListViewModel;
@class Movie;
@protocol ListViewControllerDelegate;

@interface ListViewController : UITableViewController

@property (nonatomic) ListViewModel *viewModel;
@property (nonatomic, weak) id <ListViewControllerDelegate> delegate;

@end

@protocol ListViewControllerDelegate <NSObject>

- (void)listViewController:(ListViewController *)listViewController didSelectDetailWithMovie:(Movie *)movie;
- (void)listViewController:(ListViewController *)listViewController didSelectPlayerWithMovie:(Movie *)movie;

@end
