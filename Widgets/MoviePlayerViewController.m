//
//  MoviePlayerViewController.m
//  Widgets
//
//  Created by Hu Junfeng on 8/5/15.
//  Copyright (c) 2015 2359 Media Pte Ltd. All rights reserved.
//

#import "MoviePlayerViewController.h"
#import "MoviePlayerViewModel.h"
#import "Movie.h"

@interface MoviePlayerViewController ()

@property (weak, nonatomic) IBOutlet UILabel *movieTitleLabel;

@end

@implementation MoviePlayerViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self bindViewModel];
}

- (void)bindViewModel
{
    self.title = self.viewModel.title;
    self.movieTitleLabel.text = self.viewModel.movie.title;
}

- (IBAction)close:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(moviePlayerViewControllerDidClose:)]) {
        [self.delegate moviePlayerViewControllerDidClose:self];
    }
}

@end
