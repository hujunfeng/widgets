//
//  RootWidget.h
//  Widgets
//
//  Created by Hu Junfeng on 8/5/15.
//  Copyright (c) 2015 2359 Media Pte Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@class UIViewController;

@interface RootWidget : NSObject

@property (nonatomic, readonly) UIViewController *initialViewController;

@end
