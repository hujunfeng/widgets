//
//  ViewModel.h
//  Widgets
//
//  Created by Hu Junfeng on 13/5/15.
//  Copyright (c) 2015 2359 Media Pte Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ViewModel <NSObject>

- (instancetype)initWithModel:(id)model;

@end
