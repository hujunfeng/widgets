//
//  RootWidget.m
//  Widgets
//
//  Created by Hu Junfeng on 8/5/15.
//  Copyright (c) 2015 2359 Media Pte Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "RootWidget.h"

#import "Movie.h"
#import "IMDb.h"

#import "ListViewModel.h"
#import "ListViewController.h"
#import "MovieDetailViewModel.h"
#import "MovieDetailViewController.h"
#import "MoviePlayerViewModel.h"
#import "MoviePlayerViewController.h"

@interface RootWidget () <ListViewControllerDelegate, MovieDetailViewControllerDelegate, MoviePlayerViewControllerDelegate>

@property (nonatomic) UINavigationController *navigationController;
@property (nonatomic) UIViewController *listViewController;

@end

@implementation RootWidget

- (instancetype)init
{
    self = [super init];
    if (self) {
    }
    return self;
}

#pragma mark - View Controllers

- (UIViewController *)listViewController
{
    if (!_listViewController) {
        RACSignal *signal = [[IMDb sharedStore] signalForTopMovies];
        ListViewModel *listViewModel = [[ListViewModel alloc] initWithModel:signal];
        ListViewController *viewController = [[ListViewController alloc] init];
        viewController.viewModel = listViewModel;
        viewController.delegate = self;
        _listViewController = viewController;
    }
    return _listViewController;
}

- (UIViewController *)movieDetailViewControllerWithMovie:(Movie *)movie
{
    MovieDetailViewModel *viewModel = [[MovieDetailViewModel alloc] initWithModel:movie];
    
    // MovieDetailViewController can be initialized with different ways, for example XIB files, Storyboards, etc.
    
    // 1. Init from a default XIB file (check `nibName` property of UIViewController for what a default name should be)
    MovieDetailViewController *viewController = [[MovieDetailViewController alloc] init];
    
    // 2. Init from another XIB file
//    MovieDetailViewController *viewController = [[MovieDetailViewController alloc] initWithNibName:@"MovieDetailViewControllerAnother" bundle:nil];
    
    viewController.viewModel = viewModel;
    viewController.delegate = self;

    return viewController;
}

- (UIViewController *)moviePlayerViewControllerWithMovie:(Movie *)movie
{
    MoviePlayerViewModel *viewModel = [[MoviePlayerViewModel alloc] initWithModel:movie];
    MoviePlayerViewController *viewController = [[MoviePlayerViewController alloc] init];
    viewController.viewModel = viewModel;
    viewController.delegate = self;
    
    return viewController;
}

- (UINavigationController *)navigationController
{
    if (!_navigationController) {
        _navigationController = [[UINavigationController alloc] initWithRootViewController:self.listViewController];
    }
    return _navigationController;
}

- (UIViewController *)initialViewController
{
    return self.navigationController;
}

#pragma mark - ListViewControllerDelegate

- (void)listViewController:(ListViewController *)listViewController didSelectDetailWithMovie:(Movie *)movie
{
    [self showMovieDetailWithMovie:movie];
}

- (void)listViewController:(ListViewController *)listViewController didSelectPlayerWithMovie:(Movie *)movie
{
    [self showMoviePlayerWithMovie:movie];
}

#pragma mark - MovieDetailViewControllerDelegate

- (void)movieDetailViewController:(MovieDetailViewController *)viewController didSelectPlayerWithMovie:(Movie *)movie
{
    [self showMoviePlayerWithMovie:movie];
}

#pragma mark - MoviePlayerViewControllerDelegate

- (void)moviePlayerViewControllerDidClose:(MoviePlayerViewController *)viewController
{
    [viewController.presentingViewController dismissViewControllerAnimated:YES completion:^{
        NSLog(@"Movie Player Closed.");
    }];
}

#pragma mark - Navigation Helpers

- (void)showMovieDetailWithMovie:(Movie *)movie
{
    UIViewController *vc = [self movieDetailViewControllerWithMovie:movie];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)showMoviePlayerWithMovie:(Movie *)movie
{
    UIViewController *vc = [self moviePlayerViewControllerWithMovie:movie];
    [self.navigationController.visibleViewController presentViewController:vc animated:YES completion:nil];
}

@end
